using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    [SerializeField] private GameObject explosionVFX;
    public Player player;

    public int hitPoints = 30;
    private bool isExploded;

    public int whiteCrystalMin, whiteCrystalMax;
    public int redCrystalMin, redCrystalMax;
    public int greenCrystalMin, greenCrystalMax;


    // Start is called before the first frame update
    void Start()
    {
        transform.Rotate(0, Random.Range(20, 150), 0);
    }

    public void TakeDamage(int damage = 1)
    {
        if (isExploded) return;

        hitPoints -= damage;
        if(hitPoints <= 0)
        {
            isExploded = true;
            player.whiteCrystalAmount += Random.Range(whiteCrystalMin, whiteCrystalMax);
            player.redCrystalAmount += Random.Range(redCrystalMin, redCrystalMax);
            player.greenCrystalAmount += Random.Range(greenCrystalMin, greenCrystalMax);
            player.score += 3000;
            player.UpdateUI();

            Instantiate(explosionVFX, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
