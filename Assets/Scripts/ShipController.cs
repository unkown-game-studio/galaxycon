using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour
{
    [SerializeField] private FloatingJoystick joystick;

    public float speed = 1500f;
    public float rotationSpeed = 20f;
    public float rotationRange = 0.5f;
    public float rollRange = 45f;
    public float rollSpeed = 100f;

    private Rigidbody _body;
    private Player _player;

    private float _horizontal;
    private float _vertical;

    private Vector3 _direction;
    public Vector3 Direction { get { return _direction; } set { _direction = value; } }

    void Start()
    {
        _body = GetComponent<Rigidbody>();
        _player = GameObject.Find("Player").GetComponent<Player>();
    }

    void Update()
    {
        if(Config.MOBILE_PLATFORM)
        {
            if(joystick.enabled)
            {
                _horizontal = joystick.Horizontal;
                _vertical = joystick.Vertical;
            }
            else
            {
                _horizontal = _vertical = 0;
            }
        }
        else
        {
            _horizontal = Input.GetAxis("Horizontal");
            _vertical = Input.GetAxis("Vertical");
        }
    }

    void FixedUpdate()
    {
        _direction = new Vector3(_horizontal, 0, _vertical);
        _direction = Vector3.ClampMagnitude(_direction, 1);

        if (_player.IsDead || _player.IsDocked) return;


        float thrust = Vector3.Dot(_direction.normalized, transform.forward);
        float rotation = Vector3.Dot(_direction.normalized, transform.right);
        float roll = -rotation * rollRange;

        thrust = Mathf.Clamp(thrust, 0, 1);
        rotation = Mathf.Clamp(rotation, -rotationRange, rotationRange);

        float rotationAmount = rotation * rotationSpeed * (thrust < 0.2f ? rotationSpeed * 3 : rotationSpeed) * _direction.magnitude * Time.deltaTime;

        Quaternion targetRotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y + rotationAmount, roll);

        if (Mathf.Approximately(rotation, 0))
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rollSpeed * Time.deltaTime);
        }
        else
        {
            transform.rotation = targetRotation;
        }

        _body.AddForce(transform.forward * _direction.magnitude * thrust * speed * Time.deltaTime);
    }
}
