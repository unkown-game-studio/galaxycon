using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WandererZone : MonoBehaviour
{
    void Start()
    {
        if (transform.parent.GetComponent<EnemyMovement>().type == EnemyMovement.Type.Defender)
            Destroy(gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            transform.parent.GetComponent<EnemyMovement>().WandererPlayerDetected();
    }
}
