using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeShip : MonoBehaviour
{
    [SerializeField] private GameObject[] parts;

    void Start()
    {
        Destroy(gameObject, 4f);
    }

    void FixedUpdate()
    {
        foreach(GameObject part in parts) {
            part.GetComponent<Rigidbody>().AddExplosionForce(15f, transform.position, 5f);
        }
    }
}
