using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dock : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        Player player = other.transform.parent?.GetComponent<Player>();
        if (player == null) return;

        player.GetComponent<Rigidbody>().velocity = Vector3.zero;
        player.EnterDock(transform.position);
    }
}
