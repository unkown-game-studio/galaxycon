using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingWeapons : MonoBehaviour
{
    private Transform ship;
    private FixedJoystick fireJoystick;
    public float rotationRange = 90f;

    void Awake()
    {
        ship = GameObject.Find("Player").transform;
        fireJoystick = ship.GetComponent<Player>().fireJoystick;
    }

    void Update()
    {
        if (Config.MOBILE_PLATFORM)
        {
            Vector3 fireDirection = new Vector3(fireJoystick.Horizontal, 0, fireJoystick.Vertical);
            fireDirection = Vector3.ClampMagnitude(fireDirection, 1);

            if (fireDirection.magnitude >= 0.2f)
            {
                Quaternion rotation = Quaternion.LookRotation(fireDirection);
                float yRotation = rotation.eulerAngles.y - ship.rotation.eulerAngles.y;

                if ((yRotation >= rotationRange && yRotation <= 180f)
                    || (yRotation <= -180f && yRotation >= rotationRange - 360f))
                {
                    yRotation = rotationRange;
                }

                else if ((yRotation > 180f && yRotation <= 360 - rotationRange)
                    || (yRotation <= -rotationRange && yRotation > -180f))
                {
                    yRotation = -rotationRange;
                }

                transform.rotation = Quaternion.Euler(rotation.x, yRotation + ship.rotation.eulerAngles.y, rotation.z);
            }
            else
            {
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(ship.forward), 2f);
            }
        }
        else
        {
            transform.rotation = Quaternion.LookRotation(ship.forward);
        }
    }
}
