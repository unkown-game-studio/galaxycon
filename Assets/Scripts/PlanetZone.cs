using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetZone : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            transform.parent.GetComponent<Planet>().PlayerEnteredZone();
    }
}
