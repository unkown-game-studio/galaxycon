using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHoleCenter : MonoBehaviour
{
    public BlackHole blackHoleZone;
    public Transform[] teleportPoints;
    void OnTriggerEnter(Collider other)
    {
        Player player = other.GetComponent<Player>();
        EnemyMovement enemy = other.GetComponent<EnemyMovement>();

        if (player != null && blackHoleZone.pulledBodies.Contains(player.GetComponent<Rigidbody>()))
        {
            blackHoleZone.pulledBodies.Remove(player.GetComponent<Rigidbody>());
            player.transform.position = teleportPoints[Random.Range(0, teleportPoints.Length - 1)].position;
            player.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }

        if (enemy != null && blackHoleZone.pulledBodies.Contains(enemy.GetComponent<Rigidbody>()))
        {
            blackHoleZone.pulledBodies.Remove(enemy.GetComponent<Rigidbody>());
            enemy.transform.position = teleportPoints[Random.Range(0, teleportPoints.Length - 1)].position;
            enemy.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }
}
