using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject MapUI;
    [SerializeField] private TMP_Text scoreText;
    [SerializeField] private Player player;

    public void OpenMAPUI()
    {
        MapUI.SetActive(true);
        scoreText.text = "Score: " + player.score;
    }

    public void CloseMAPUI()
    {
        MapUI.SetActive(false);
    }
}
