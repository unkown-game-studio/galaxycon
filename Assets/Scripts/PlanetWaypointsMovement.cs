using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetWaypointsMovement : MonoBehaviour
{
    public float speed = 1f;

    void Update()
    {
        transform.Rotate(0, Time.deltaTime * speed, 0);
    }
}
