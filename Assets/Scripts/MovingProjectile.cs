using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingProjectile : MonoBehaviour
{
    public float speed = 20f;
    public int destroyTime = 5;
    public int damage;

    [SerializeField] private GameObject hitPrefab;
    [SerializeField] private AudioClip hitSound;
    [SerializeField] private AudioSource audioSource;

    void Start()
    {
        Destroy(gameObject, destroyTime);
    }

    void Update()
    {
        transform.position += transform.forward * speed * Time.deltaTime;
    }

    void OnCollisionEnter(Collision collision)
    {
        speed = 0;

        ContactPoint cp = collision.contacts[0];

        Quaternion rot = Quaternion.FromToRotation(Vector3.up, cp.normal);
        Vector3 pos = cp.point;

        Instantiate(hitPrefab, pos, rot);
        audioSource.PlayOneShot(hitSound);
        
        Player player = cp.otherCollider.transform.parent?.GetComponent<Player>();
        EnemyMovement enemy = cp.otherCollider.GetComponent<EnemyMovement>();
        Asteroid asteroid = cp.otherCollider.GetComponent<Asteroid>();

        if (player != null) player.TakeDamage(damage);
        if (enemy != null) enemy.TakeDamage(damage);
        if (asteroid != null) asteroid.TakeDamage();

        Destroy(gameObject, 0.4f);
    }
}
