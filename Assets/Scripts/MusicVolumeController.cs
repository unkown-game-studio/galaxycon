using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicVolumeController : MonoBehaviour
{
    [SerializeField] private AudioSource musicSource;

    void Start()
    {
        musicSource.ignoreListenerVolume = true;
        musicSource.volume = PlayerPrefs.GetFloat("musicvolume", 0.05f);
        AudioListener.volume = PlayerPrefs.GetFloat("soundvolume", 0.15f);
    }
}
