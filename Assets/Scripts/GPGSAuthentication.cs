using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class GPGSAuthentication : MonoBehaviour
{
    public static PlayGamesPlatform platform;

    public static bool isConnectedToGPGS = false;
    [SerializeField] private Player player;

    void Start()
    {
        if (platform == null)
        {
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
            PlayGamesPlatform.InitializeInstance(config);
            PlayGamesPlatform.DebugLogEnabled = true;

            platform = PlayGamesPlatform.Activate();
        }

        SignInToGPGS();
    }

    void SignInToGPGS()
    {
        PlayGamesPlatform.Instance.Authenticate(SignInInteractivity.CanPromptOnce, (result) =>
        {
            switch (result)
            {
                case SignInStatus.Success:
                    isConnectedToGPGS = true;
                    break;
                default:
                    isConnectedToGPGS = false;
                    break;
            }
        });
    }

    public void ShowLeaderBoard()
    {
        if (isConnectedToGPGS)
        {
            Social.ShowLeaderboardUI();
        }
    }

    public void SubmitScore()
    {
        if (isConnectedToGPGS)
        {
            Social.ReportScore(player.score, GPGSIds.leaderboard_score_table, (bool success) =>
            {

            });
        }
    }
}
