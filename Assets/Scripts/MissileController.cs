using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileController : MonoBehaviour
{
    public MissileSpawner[] missileSpawners;

    public MissileSpawner[] GetMissileSpawners()
    {
        return missileSpawners;
    }
}
