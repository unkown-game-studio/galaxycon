using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Player : MonoBehaviour
{ 
    public int[] healthLevels = new int[] { 200, 400, 650, 900, 1100 };
    public int[] damageLevels = new int[] { 15, 20, 35, 45, 60 };
    public int[] fireRateLevels = new int[] { 4, 6, 6, 8, 12 };
    public float[] speedLevels = new float[] { 1600f, 1800f, 2000f, 2300f, 2500f };
    public float[] rotationSpeedLevels = new float[] { 20f, 22f, 23f, 24f, 26f };

    public int shipLevel = 1;
    public int weaponLevel = 1;
    public int speedLevel = 1;
    public int score;
    public int whiteCrystalAmount;
    public int redCrystalAmount;
    public int greenCrystalAmount;
    public Vector3 lastDockPosition;

    private int _health;
    private int _maxHealth;
    public int weaponDamage;
    public int fireRate;

    [SerializeField] private GameObject[] brokenShipPrefabs;
    [SerializeField] private GameObject[] shipPrefabs;
    [SerializeField] private GameObject bombPrefab;
    [SerializeField] private HealthBar healthBar;
    [SerializeField] private GameObject gameHUD;
    [SerializeField] private GameObject marketUI;
    [SerializeField] private TMP_Text whiteCrystalText;
    [SerializeField] private TMP_Text redCrystalText;
    [SerializeField] private TMP_Text greenCrystalText;

    [SerializeField] private Button missileButton;
    [SerializeField] private Button boostButton;
    [SerializeField] private Button explosiveButton;

    [SerializeField] private Market market;
    [SerializeField] private GameManager gameManager;

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioSource thrusterSource;

    [SerializeField] private AudioClip boostSound;
    [SerializeField] private GameObject boostVFXPrefab;

    private MissileSpawner[] missileSpawners;
    private ShipController shipController;
    private Rigidbody rigidBody;

    public List<EnemyMovement> chasers = new List<EnemyMovement>();

    public FloatingJoystick movementJoystick;
    public FixedJoystick fireJoystick;
    
    private bool _isDocked = false;
    public bool IsDead { get { return _health <= 0; } }
    public bool IsDocked { get { return _isDocked; } }

    public float missileCooldown = 2.5f;
    public float boostCooldown = 10f;
    public float bombCoolDown = 10f;
    public float boostDuration = 2f;

    public float minThrusterPitch = 0.3f;
    public float maxThrusterPitch = 2.5f;

    void Start()
    {
        shipController = GetComponent<ShipController>();
        rigidBody = GetComponent<Rigidbody>();

        thrusterSource.pitch = minThrusterPitch;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) FireMissiles();
        float speed = rigidBody.velocity.magnitude;

        thrusterSource.pitch = (speed / 135) * (maxThrusterPitch - minThrusterPitch) + minThrusterPitch;
    }

    void LateUpdate()
    {
        if (Mathf.Abs(transform.position.y) >= 0.5f)
            transform.position = new Vector3(transform.position.x, 0, transform.position.z);
    }

    public void SpawnShip()
    {
        GameObject ship = Instantiate(shipPrefabs[shipLevel - 1], transform.position, Quaternion.identity);
        ship.transform.parent = transform;
        ship.transform.localRotation = Quaternion.identity;
        thrusterSource.Play();

        MissileController missileController = ship.GetComponent<MissileController>();
        if (missileController != null) missileSpawners = missileController.GetMissileSpawners();

        _maxHealth = healthLevels[shipLevel - 1];
        weaponDamage = damageLevels[weaponLevel - 1];
        fireRate = fireRateLevels[weaponLevel - 1];
        shipController.speed = speedLevels[speedLevel - 1];
        shipController.rotationSpeed = rotationSpeedLevels[speedLevel - 1];

        _health = _maxHealth;

        UpdateUI();
    }

    public void EnterDock(Vector3 dockPosition)
    {
        if (chasers.Count != 0) return;

        movementJoystick.OnPointerUp(null);
        fireJoystick.OnPointerUp(null);

        _isDocked = true;
        gameHUD.SetActive(false);
        marketUI.SetActive(true);
        market.UpdateUI();

        transform.position = new Vector3(dockPosition.x, transform.position.y, dockPosition.z);
        transform.rotation = Quaternion.LookRotation(new Vector3(1, 0, -1));

        lastDockPosition = transform.position;

        _health = _maxHealth;
    }

    public void LeaveDock()
    {
        _isDocked = false;
        gameHUD.SetActive(true);
        marketUI.SetActive(false);
        UpdateUI();
    }

    public void FireMissiles()
    {
        StartCoroutine(FireMissilesRoutine());
    }

    IEnumerator FireMissilesRoutine()
    { 
        if (chasers.Count != 0) {
            missileButton.interactable = false;

            for (int i = 0; i < missileSpawners.Length; i++) 
                missileSpawners[i].SpawnMissile(chasers[0].transform);

            yield return new WaitForSeconds(missileCooldown);

            missileButton.interactable = true;
        }
    }

    public void BoostSpeed()
    {
        StartCoroutine(BoostSpeedRoutine());
    }

    IEnumerator BoostSpeedRoutine()
    {
        boostButton.interactable = false;
        audioSource.PlayOneShot(boostSound);
        GameObject boostVFX = Instantiate(boostVFXPrefab, transform.position, transform.rotation);
        boostVFX.transform.parent = transform;

        GetComponent<ShipController>().speed = speedLevels[speedLevel - 1] * 1.5f;

        yield return new WaitForSeconds(boostDuration);

        GetComponent<ShipController>().speed = speedLevels[speedLevel - 1];
        Destroy(boostVFX);

        yield return new WaitForSeconds(boostCooldown);

        boostButton.interactable = true;
    }
 
    public void SpawnBomb()
    {
        StartCoroutine(SpawnBombRoutine());
    }

    IEnumerator SpawnBombRoutine()
    {
        explosiveButton.interactable = false;

        Instantiate(bombPrefab, transform.TransformPoint(Vector3.back * 30f), Quaternion.identity);

        yield return new WaitForSeconds(bombCoolDown);

        explosiveButton.interactable = true;
    }

    public void TakeDamage(int damage) {
        _health -= damage;
        healthBar.SetHealth(_health); 

        if(_health <= 0) {
            StartCoroutine(DestroyShip());
        }
    }

    public void UpdateUI()
    {
        whiteCrystalText.text = whiteCrystalAmount.ToString();
        greenCrystalText.text = greenCrystalAmount.ToString();
        redCrystalText.text = redCrystalAmount.ToString();

        missileButton.interactable = shipLevel >= 3;
        boostButton.interactable = shipLevel >= 4;
        explosiveButton.interactable = shipLevel == 5;

        healthBar.GetComponent<Slider>().maxValue = _maxHealth;
        healthBar.SetMaxHealth(_maxHealth);
    }

    IEnumerator DestroyShip() {
        if (!IsDead) yield break;
        _health = _maxHealth;
        thrusterSource.Stop();

        movementJoystick.OnPointerUp(null);
        fireJoystick.OnPointerUp(null);
        movementJoystick.enabled = false;
        fireJoystick.enabled = false;

        Transform oldShip = transform.GetChild(0);
        Destroy(oldShip.gameObject);

        GameObject brokenShip = Instantiate(brokenShipPrefabs[shipLevel - 1], transform.position, oldShip.rotation);

        foreach (EnemyMovement enemy in chasers)
            enemy.StopChasingPlayer();

        yield return new WaitForSeconds(4);

        chasers.Clear();

        Destroy(brokenShip);
        transform.position = lastDockPosition;

        SpawnShip();

        fireJoystick.enabled = true;
        movementJoystick.enabled = true;
    }

    public void DebugAddMoney()
    {
        whiteCrystalAmount += 10000;
        redCrystalAmount += 10000;
        greenCrystalAmount += 10000;

        UpdateUI();
    }

    public void DebugReset()
    {
        whiteCrystalAmount = 0;
        redCrystalAmount = 0;
        greenCrystalAmount = 0;

        shipLevel = 1;
        weaponLevel = 1;
        speedLevel = 1;

        Transform oldShip = transform.GetChild(0);
        Destroy(oldShip.gameObject);

        foreach(Planet planet in gameManager.planets)
        {
            planet.isCaptured = false;
            planet.zone.SetActive(true);
            planet.dock.SetActive(false);
        }

        SpawnShip();

        UpdateUI();
    }
}
