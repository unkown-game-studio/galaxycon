using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyMovement : MonoBehaviour
{
    private enum State
    {
        Patrolling, Chasing, Attacking, ChasingAndAttacking
    }

    public enum Type
    {
        Defender, Wanderer
    }

    public enum ZoneType
    {
        Normal, Cold, Hot
    }

    public float speed = 1500f;
    public float rotationSpeed = 20f;
    public float rotationRange = 0.5f;
    public float rollRange = 30f;
    public float rollSpeed = 100f;
    public float projectileBias = 10f;
    public int level = 1;

    public float patrolDistance = 240f;
    public float attackDistance = 60f;
    public float attackAndChaseDistance = 150f;

    public int maxHealth = 100;
    private int _health;
    public int weaponDamage = 10;

    public ZoneType zoneType;
    public int whiteCrystalRewardMin, whiteCrystalRewardMax;
    public int redCrystalRewardMin, redCrystalRewardMax;
    public int greenCrystalRewardMin, greenCrystalRewardMax;

    private Rigidbody _body;

    private Vector3 _directionVelocity, _targetPos;

    [SerializeField] private GameObject brokenShipPrefab;
    [SerializeField] private Slider healthBar;
    [SerializeField] private EnemyProjectileSpawner[] projectileSpawners;
    [SerializeField] private AudioSource thrusterSource;

    private Player _player;
    public Planet planet;
    public WandererEnemyManager manager;

    public Transform targetObject;

    public float fireRate = 10;
    public float evadeObstacleTime = 0.4f;
    public float evadeDistance = 10f;

    public float minThrusterPitch = 0.3f;
    public float maxThrusterPitch = 2f;

    private float timeToFire = 0;
    private float _timeToLeaveEvadeObstacleState = 0f;
    private bool _evadingObstacle = false;
    private Vector3 _evadeObstacleDestination;

    Vector3 hp;

    private State _state;
    public Type type;

    void Start()
    {
        _player = GameObject.Find("Player").GetComponent<Player>();
        _body = GetComponent<Rigidbody>();
        _state = State.Patrolling;
        _health = maxHealth;
        healthBar.maxValue = maxHealth;
        healthBar.value = _health;
        
    }
    void Update()
    {
        if (targetObject == null) return;

        _targetPos = Vector3.SmoothDamp(_targetPos, targetObject.position, ref _directionVelocity, 1f);

        float speed = _body.velocity.magnitude;

        thrusterSource.pitch = (speed / 90) * (maxThrusterPitch - minThrusterPitch) + minThrusterPitch;
        thrusterSource.pitch = minThrusterPitch;
    }

    void FixedUpdate()
    {
        if (targetObject == null) return;
        
        Vector3 directionToTarget = targetObject.position - transform.position;
        Vector3 directionToTargetFollowPoint = _targetPos - transform.position;
        float distanceToTarget = directionToTargetFollowPoint.magnitude;

        _state = State.Patrolling;

        if (type == Type.Defender)
        {
            if (distanceToTarget >= patrolDistance)
            {
                targetObject = planet.GetWayPoint();
            }
        }
        
        else if((targetObject.CompareTag("Player") && distanceToTarget >= patrolDistance) ||
               (distanceToTarget <= 1.5f * evadeDistance && targetObject.CompareTag("PatrolWayPoint")))
        { 
                targetObject = manager.GetWayPoint();
        }

        if (!targetObject.CompareTag("PatrolWayPoint") && distanceToTarget <= attackDistance)
            _state = State.Attacking;
        else if (!targetObject.CompareTag("PatrolWayPoint") && distanceToTarget >= attackDistance && distanceToTarget <= attackAndChaseDistance)
            _state = State.ChasingAndAttacking;
        else if (!targetObject.CompareTag("PatrolWayPoint") && distanceToTarget >= attackAndChaseDistance && distanceToTarget <= patrolDistance)
            _state = State.Chasing;


        if(_state == State.Patrolling)
        {
            if (_player.chasers.Contains(GetComponent<EnemyMovement>()))
            {
                _player.chasers.Remove(GetComponent<EnemyMovement>());
                _health = maxHealth;
                healthBar.value = _health;
            }
        }

        if (type == Type.Defender) { if (Vector3.Distance(transform.position, _player.transform.position) >= 600f) return; }
        else if (type == Type.Wanderer) { if (Vector3.Distance(transform.position, _player.transform.position) >= 800f) return; }

        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, evadeDistance))
        {
            if (!hit.transform.CompareTag("Projectile"))
            {
                hp = hit.point;
                _evadingObstacle = true;
                _timeToLeaveEvadeObstacleState = Time.realtimeSinceStartup + evadeObstacleTime;
                _evadeObstacleDestination = transform.position + transform.TransformDirection(new Vector3(1, 0, 1)) * 50f;
            }
        }

        if(_evadingObstacle)
        {
            directionToTarget = _evadeObstacleDestination - transform.position;

            float thrust = Vector3.Dot(directionToTarget.normalized, transform.forward);
            float rotation = Vector3.Dot(directionToTarget.normalized, transform.right);
            float roll = -rotation * rollRange;

            thrust = Mathf.Clamp(thrust, 0, 1);
            rotation = Mathf.Clamp(rotation, -rotationRange, rotationRange);

            float rotationAmount = rotation * rotationSpeed * (thrust < 0.2f ? rotationSpeed * 3 : rotationSpeed) * Time.deltaTime;

            Quaternion targetRotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y + rotationAmount, roll);

            transform.rotation = targetRotation;

            _body.AddForce(transform.forward * thrust * speed * Time.deltaTime);

            if (Time.realtimeSinceStartup >= _timeToLeaveEvadeObstacleState)
                _evadingObstacle = false;
            
            return;
        }

        switch(_state)
        {
            case State.Patrolling:
                {
                    float thrust = Vector3.Dot(directionToTarget.normalized, transform.forward);
                    float rotation = Vector3.Dot(directionToTarget.normalized, transform.right);
                    float roll = -rotation * rollRange;

                    thrust = Mathf.Clamp(thrust, 0, 1);
                    rotation = Mathf.Clamp(rotation, -rotationRange, rotationRange);

                    float rotationAmount = rotation * rotationSpeed * (thrust < 0.2f ? rotationSpeed * 3 : rotationSpeed) * Time.deltaTime;

                    Quaternion targetRotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y + rotationAmount, roll);

                    transform.rotation = targetRotation;

                    _body.AddForce(transform.forward * thrust * speed * Time.deltaTime);
                    break;
                }

            case State.Chasing:
                {
                    float thrust = Vector3.Dot(directionToTargetFollowPoint.normalized, transform.forward);
                    float rotation = Vector3.Dot(directionToTargetFollowPoint.normalized, transform.right);
                    float roll = -rotation * rollRange;

                    thrust = Mathf.Clamp(thrust, 0, 1);
                    rotation = Mathf.Clamp(rotation, -rotationRange, rotationRange);

                    float rotationAmount = rotation * rotationSpeed * (thrust < 0.2f ? rotationSpeed * 3 : rotationSpeed) * Time.deltaTime;

                    Quaternion targetRotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y + rotationAmount, roll);

                    transform.rotation = targetRotation;

                    _body.AddForce(transform.forward * thrust * speed * Time.deltaTime);
                    break;
                }
            case State.Attacking:
                {
                    
                    float rotation = Vector3.Dot(directionToTarget.normalized, transform.right);
                    float roll = -rotation * rollRange;

                    rotation = Mathf.Clamp(rotation, -rotationRange, rotationRange);

                    float rotationAmount = rotation * rotationSpeed * 20f * Time.deltaTime;

                    Quaternion targetRotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y + rotationAmount, roll);

                    transform.rotation = targetRotation;

                    if (Time.time >= timeToFire)
                    {
                        timeToFire = Time.time + 1 / fireRate + Random.Range(0.05f, 0.08f);

                        foreach(EnemyProjectileSpawner spawner in projectileSpawners)
                            spawner.Spawn(weaponDamage, transform.rotation);
                    }
                    break;
                }
            case State.ChasingAndAttacking:
                {
                    float thrust = Vector3.Dot(directionToTargetFollowPoint.normalized, transform.forward);
                    float rotation = Vector3.Dot(directionToTarget.normalized, transform.right);
                    float roll = -rotation * rollRange;

                    thrust = Mathf.Clamp(thrust, 0, 1);
                    rotation = Mathf.Clamp(rotation, -rotationRange, rotationRange);

                    float rotationAmount = rotation * rotationSpeed * 20f * Time.deltaTime;

                    Quaternion targetRotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y + rotationAmount, roll);

                    transform.rotation = targetRotation;

                    if (Time.time >= timeToFire)
                    {
                        timeToFire = Time.time + 1 / fireRate + Random.Range(0.05f, 0.08f);

                        Vector3 shipEulerAngles = transform.rotation.eulerAngles;
                        Quaternion projectileRotation = Quaternion.Euler(
                            shipEulerAngles.x,
                            shipEulerAngles.y + Random.Range(-projectileBias, projectileBias), 
                            shipEulerAngles.z);

                        foreach (EnemyProjectileSpawner spawner in projectileSpawners)
                            spawner.Spawn(weaponDamage, projectileRotation);
                    }

                    _body.AddForce(transform.forward * thrust * speed * Time.deltaTime);
                    break;
                }
        }
            
    }

    void LateUpdate()
    {
        if(Mathf.Abs(transform.position.y) >= 0.5f)
            transform.position = new Vector3(transform.position.x, 0, transform.position.z);    
    }

    public void TakeDamage(int damage) {
        _health -= damage;
        healthBar.value = _health;
        if (_health <= 0) {
            Destroy(gameObject);
            Instantiate(brokenShipPrefab, transform.position, transform.rotation);
            if(type == Type.Defender)
                planet.defenders.Remove(GetComponent<EnemyMovement>());

            if (_player.chasers.Contains(GetComponent<EnemyMovement>()))
                _player.chasers.Remove(GetComponent<EnemyMovement>());

            _player.score += level * 100;
            _player.whiteCrystalAmount += Random.Range(whiteCrystalRewardMin, whiteCrystalRewardMax);
            if(zoneType == ZoneType.Hot) _player.redCrystalAmount += Random.Range(whiteCrystalRewardMin, whiteCrystalRewardMax);
            else if(zoneType == ZoneType.Cold) _player.greenCrystalAmount += Random.Range(whiteCrystalRewardMin, whiteCrystalRewardMax);

            _player.UpdateUI();
        }
    }

    public void WandererPlayerDetected()
    {
        targetObject = _player.transform;
        _targetPos = targetObject.position;

        if (!_player.chasers.Contains(GetComponent<EnemyMovement>()))
            _player.chasers.Add(GetComponent<EnemyMovement>());
    }

    public void StopChasingPlayer()
    {
        targetObject = type == Type.Defender ? planet.GetWayPoint() : manager.GetWayPoint();
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, transform.position + transform.forward * 20f);
        Gizmos.DrawSphere(hp, 4f);
        Gizmos.DrawSphere(_evadeObstacleDestination, 2f);
    }
}
