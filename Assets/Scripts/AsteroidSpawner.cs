using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawner : MonoBehaviour
{
    [SerializeField] private GameObject asteroidPrefab;
    [SerializeField] private Player player;

    public GameObject[] spawnPoints;
    public List<Asteroid> spawnedAsteroids;

    public float spawnDelay = 120f;
    private float nextTimeToSpawn;

    void Start()
    {
        spawnedAsteroids = new List<Asteroid>(spawnPoints.Length);
        foreach (GameObject point in spawnPoints)
        {
            Asteroid asteroid = Instantiate(asteroidPrefab, point.transform.position, Quaternion.identity).GetComponent<Asteroid>();
            asteroid.player = player;
            spawnedAsteroids.Add(asteroid);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.realtimeSinceStartup >= nextTimeToSpawn)
        {
            nextTimeToSpawn = Time.realtimeSinceStartup + spawnDelay;
            for (int i = 0; i < spawnPoints.Length; i++)
                if (spawnedAsteroids[i] == null)
                {
                    Asteroid asteroid = Instantiate(asteroidPrefab, spawnPoints[i].transform.position, Quaternion.identity).GetComponent<Asteroid>();
                    asteroid.player = player;
                    spawnedAsteroids[i] = asteroid;
                }
                        
        }
    }
}
