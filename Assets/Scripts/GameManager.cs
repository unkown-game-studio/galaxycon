using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class GameManager : MonoBehaviour
{
    [SerializeField] private Player player;
    [SerializeField] private GPGSAuthentication gpgs;
    public Planet[] planets;
   
    void Start()
    {
        LoadGame();
    }

    private void OnApplicationPause()
    {
        SaveGame();
        gpgs.SubmitScore();
    }

    void SaveGame()
    {
        using (BinaryWriter writer = new BinaryWriter(File.Open(Application.persistentDataPath + "/savegame.dat", FileMode.OpenOrCreate)))
        {
            SaveData data = new SaveData();
            data.playerScore = player.score;
            data.whiteCrystalAmount = player.whiteCrystalAmount;
            data.redCrystalAmount = player.redCrystalAmount;
            data.greenCrystalAmount = player.greenCrystalAmount;
            data.shipLevel = player.shipLevel;
            data.weaponLevel = player.weaponLevel;
            data.speedLevel = player.speedLevel;
            data.lastDockPosition = player.lastDockPosition;

            bool[] planetCaptureStates = new bool[planets.Length];
            for (int i = 0; i < planets.Length; i++) planetCaptureStates[i] = planets[i].isCaptured;
            data.planetCaptureStates = planetCaptureStates;

            writer.Write(JsonUtility.ToJson(data));
        }
    }

    void LoadGame()
    {
        if(File.Exists(Application.persistentDataPath + "/savegame.dat"))
        {
            using (BinaryReader reader = new BinaryReader(File.Open(Application.persistentDataPath + "/savegame.dat", FileMode.Open)))
            {
                SaveData data = JsonUtility.FromJson<SaveData>(reader.ReadString());
                player.score = data.playerScore;
                player.whiteCrystalAmount = data.whiteCrystalAmount;
                player.redCrystalAmount = data.redCrystalAmount;
                player.greenCrystalAmount = data.greenCrystalAmount;
                player.shipLevel = data.shipLevel;
                player.weaponLevel = data.weaponLevel;
                player.speedLevel = data.speedLevel;

                for (int i = 0; i < planets.Length; i++) planets[i].isCaptured = data.planetCaptureStates[i];

                if (data.lastDockPosition != Vector3.zero)
                    player.transform.position = data.lastDockPosition;
            }
        }

        player.SpawnShip();
        player.UpdateUI();
    }

    public void GoMainMenu()
    {
        SaveGame();
        SceneManager.LoadScene(0);
    }
}

[System.Serializable]
class SaveData
{
    public int playerScore;
    public bool[] planetCaptureStates;
    public int whiteCrystalAmount;
    public int redCrystalAmount;
    public int greenCrystalAmount;
    public int shipLevel;
    public int weaponLevel;
    public int speedLevel;
    public Vector3 lastDockPosition;
}
