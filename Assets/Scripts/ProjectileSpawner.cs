using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProjectileSpawner : MonoBehaviour
{
    [SerializeField] private GameObject projectilePrefab;
    [SerializeField] private AudioClip blasterSound;
    [SerializeField] private AudioSource soundSource;

    private Player _player;
    private FixedJoystick fireJoystick;

    private float timeToFire = 0;

    void Awake()
    {
        _player = GameObject.Find("Player").GetComponent<Player>();
        fireJoystick = _player.fireJoystick;
    }

    void Update()
    {
        Vector3 fireDirection = Vector3.forward;

        if(Config.MOBILE_PLATFORM)
        {
            fireDirection = new Vector3(fireJoystick.Horizontal, 0, fireJoystick.Vertical);
            fireDirection = Vector3.ClampMagnitude(fireDirection, 1);
        }

        if ((Input.GetKey(KeyCode.Space) || fireDirection.magnitude >= 0.4f) 
            && Time.time >= timeToFire)
        {
            soundSource.PlayOneShot(blasterSound);
            timeToFire = Time.time + 1f / _player.fireRate + Random.Range(0.05f, 0.08f);

            GameObject projectile = Instantiate(projectilePrefab);
            projectile.GetComponent<MovingProjectile>().damage = _player.weaponDamage;
            projectile.layer = LayerMask.NameToLayer("Default");
            projectile.transform.position = transform.TransformPoint(Vector3.forward * 3f);
            projectile.transform.rotation = transform.rotation;
        }
    }
}
