using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WandererEnemyManager : MonoBehaviour
{

    public int maxLevel = 5, wandererCount = 20;
    public List<EnemyMovement> wanderers;
    public Transform[] patrolWaypoints;
    public Transform[] spawnPoints;
    public GameObject[] prefabs;

    public float timeToReassignWayPoints = 10f;
    private float timePassed = 0;

    public EnemyMovement.ZoneType[] zoneTypes = new EnemyMovement.ZoneType[] { 
        EnemyMovement.ZoneType.Normal, 
        EnemyMovement.ZoneType.Cold, 
        EnemyMovement.ZoneType.Hot 
    };

    void Start()
    {
        wanderers = new List<EnemyMovement>(wandererCount);

        for (int i = 0; i < wandererCount; ++i) 
            wanderers.Add(null);
           
        SpawnWanderers();
    }

    void Update()
    {
        timePassed += Time.deltaTime;
        if (timePassed >= timeToReassignWayPoints)
        {
            timePassed = 0f;
            SpawnWanderers();

            foreach (EnemyMovement enemy in wanderers)
            {
                if (!enemy.targetObject.CompareTag("Player"))
                    enemy.targetObject = GetWayPoint();
            }
        }
    }

    public Transform GetWayPoint()
    {
        return patrolWaypoints[(int)Random.Range(0, patrolWaypoints.Length - 1)];
    }

    private void SpawnWanderers()
    {
        for (int i = 0; i < wandererCount; ++i)
        {
            if (wanderers[i] != null) continue;

            GameObject prefab = prefabs[Random.Range(1, maxLevel) - 1];
            GameObject enemyInstance = Instantiate(prefab, spawnPoints[i].position, spawnPoints[i].rotation);
            EnemyMovement enemy = enemyInstance.GetComponent<EnemyMovement>();

            enemy.type = EnemyMovement.Type.Wanderer;
            enemy.manager = gameObject.GetComponent<WandererEnemyManager>();
            enemy.targetObject = GetWayPoint();
            enemy.zoneType = zoneTypes[Random.Range(0, zoneTypes.Length - 1)];

            wanderers[i] = enemy;
        }
    }
}
