using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowingCamera : MonoBehaviour
{
    [SerializeField] private Player player;
    public float offsetZ = -20f, offsetY = 40f, dockOffsetX = 40f, dockOffsetY = -20f, dockOffsetZ = 10f;
    public float maxOffsetY = 10f, offsetZDownDirection = 40f;
    public float smoothTime = 0.2f;

    private Vector3 _velocity;

    void LateUpdate()
    {
        ShipController shipController = player.GetComponent<ShipController>();
        float yOffset = Mathf.Clamp(shipController.Direction.magnitude, 0, 1) * maxOffsetY;
        float zMagnitude = Mathf.Clamp(shipController.Direction.z, -1f, 0);

        Vector3 targetPos = new Vector3(
            player.transform.position.x + (player.IsDocked ? dockOffsetX : 0),
            offsetY + yOffset + (player.IsDocked ? dockOffsetY : 0),
            player.transform.position.z + offsetZ + (player.IsDocked ? dockOffsetZ : 0)
            + offsetZDownDirection * zMagnitude);
            ;

        transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref _velocity, smoothTime);
    }
}
