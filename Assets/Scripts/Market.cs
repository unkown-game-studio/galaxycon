using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Market : MonoBehaviour
{
    public int[][] bodyPrices = new int[][] 
    { new int[]{ 3500, 0, 0 }, new int[] { 7000, 3000, 3000 },
      new int[]{ 10000, 10000, 10000 }, new int[]{ 40000, 40000, 40000 }};

    public int[][] weaponPrices = new int[][]
    { new int[]{ 1500, 0, 0 }, new int[] { 5000, 0, 0 },
      new int[]{ 8000, 5000, 3000 }, new int[]{ 15000, 20000, 20000 }};

    public int[][] speedPrices = new int[][]
    { new int[]{ 2000, 0, 0 }, new int[] { 4000, 1000, 500 },
      new int[]{ 7000, 2000, 2000 }, new int[]{ 15000, 20000, 20000 }};

    [SerializeField] private TMP_Text whiteCrystalText;
    [SerializeField] private TMP_Text redCrystalText;
    [SerializeField] private TMP_Text greenCrystalText;

    [SerializeField] private TMP_Text bodyLevelText;
    [SerializeField] private TMP_Text weaponLevelText;
    [SerializeField] private TMP_Text speedLevelText;
    
    [SerializeField] private TMP_Text bodyWhitePriceText;
    [SerializeField] private TMP_Text bodyRedPriceText;
    [SerializeField] private TMP_Text bodyGreenPriceText;

    [SerializeField] private TMP_Text weaponWhitePriceText;
    [SerializeField] private TMP_Text weaponRedPriceText;
    [SerializeField] private TMP_Text weaponGreenPriceText;

    [SerializeField] private TMP_Text speedWhitePriceText;
    [SerializeField] private TMP_Text speedRedPriceText;
    [SerializeField] private TMP_Text speedGreenPriceText;

    [SerializeField] private Slider bodyLevelSlider;
    [SerializeField] private Slider weaponLevelSlider;
    [SerializeField] private Slider speedLevelSlider;

    [SerializeField] private Button bodyUpgradeBtn;
    [SerializeField] private Button weaponUpgradeBtn;
    [SerializeField] private Button speedUpgradeBtn;


    [SerializeField] private Player player;
    
    public void UpdateUI()
    {
        whiteCrystalText.text = player.whiteCrystalAmount.ToString();
        greenCrystalText.text = player.greenCrystalAmount.ToString();
        redCrystalText.text = player.redCrystalAmount.ToString();

        bodyLevelSlider.value = player.shipLevel;
        weaponLevelSlider.value = player.weaponLevel;
        speedLevelSlider.value = player.speedLevel;

        if (player.shipLevel < 5)
        {
            bodyLevelText.text = "Level " + player.shipLevel;
            bodyWhitePriceText.text = bodyPrices[player.shipLevel - 1][0].ToString();
            bodyRedPriceText.text = bodyPrices[player.shipLevel - 1][1].ToString();
            bodyGreenPriceText.text = bodyPrices[player.shipLevel - 1][2].ToString();
            bodyUpgradeBtn.interactable = true;
        }
        else {
            bodyLevelText.text = "Max Level";
            bodyWhitePriceText.text = "-";
            bodyRedPriceText.text = "-";
            bodyGreenPriceText.text = "-";
            bodyUpgradeBtn.interactable = false;
        }

        if (player.weaponLevel < 5)
        {
            weaponLevelText.text = "Level " + player.weaponLevel;
            weaponWhitePriceText.text = weaponPrices[player.weaponLevel - 1][0].ToString();
            weaponRedPriceText.text = weaponPrices[player.weaponLevel - 1][1].ToString();
            weaponGreenPriceText.text = weaponPrices[player.weaponLevel - 1][2].ToString();
            weaponUpgradeBtn.interactable = true;
        }
        else
        {
            weaponLevelText.text = "Max Level";
            weaponWhitePriceText.text = "-";
            weaponRedPriceText.text = "-";
            weaponGreenPriceText.text = "-";
            weaponUpgradeBtn.interactable = false;
        }

        if (player.speedLevel < 5)
        {
            speedLevelText.text = "Level " + player.speedLevel;
            speedWhitePriceText.text = speedPrices[player.speedLevel - 1][0].ToString();
            speedRedPriceText.text = speedPrices[player.speedLevel - 1][1].ToString();
            speedGreenPriceText.text = speedPrices[player.speedLevel - 1][2].ToString();
            speedUpgradeBtn.interactable = true;
        }
        else
        {
            speedLevelText.text = "Max Level";
            speedWhitePriceText.text = "-";
            speedRedPriceText.text = "-";
            speedGreenPriceText.text = "-";
            speedUpgradeBtn.interactable = false;
        }
    }

    public void UpgradeBody()
    {
        int whitePrice = bodyPrices[player.shipLevel - 1][0];
        int redPrice = bodyPrices[player.shipLevel - 1][1];
        int greenPrice = bodyPrices[player.shipLevel - 1][2];

        if(player.whiteCrystalAmount >= whitePrice 
            && player.redCrystalAmount >= redPrice 
            && player.greenCrystalAmount >= greenPrice)
        {
            Transform oldShip = player.transform.GetChild(0);
            Destroy(oldShip.gameObject);

            player.shipLevel++;
            player.whiteCrystalAmount -= whitePrice;
            player.redCrystalAmount -= redPrice;
            player.greenCrystalAmount -= greenPrice;
            player.SpawnShip();

            UpdateUI();
        }
    }

    public void UpgradeWeapons()
    {
        int whitePrice = weaponPrices[player.weaponLevel - 1][0];
        int redPrice = weaponPrices[player.weaponLevel - 1][1];
        int greenPrice = weaponPrices[player.weaponLevel - 1][2];

        if (player.whiteCrystalAmount >= whitePrice
            && player.redCrystalAmount >= redPrice
            && player.greenCrystalAmount >= greenPrice)
        {
            Transform oldShip = player.transform.GetChild(0);
            Destroy(oldShip.gameObject);

            player.weaponLevel++;
            player.whiteCrystalAmount -= whitePrice;
            player.redCrystalAmount -= redPrice;
            player.greenCrystalAmount -= greenPrice;
            player.SpawnShip();

            UpdateUI();
        }
    }

    public void UpgradeSpeed()
    {
        int whitePrice = speedPrices[player.speedLevel - 1][0];
        int redPrice = speedPrices[player.speedLevel - 1][1];
        int greenPrice = speedPrices[player.speedLevel - 1][2];

        if (player.whiteCrystalAmount >= whitePrice
            && player.redCrystalAmount >= redPrice
            && player.greenCrystalAmount >= greenPrice)
        {
            Transform oldShip = player.transform.GetChild(0);
            Destroy(oldShip.gameObject);

            player.speedLevel++;
            player.whiteCrystalAmount -= whitePrice;
            player.redCrystalAmount -= redPrice;
            player.greenCrystalAmount -= greenPrice;
            player.SpawnShip();

            UpdateUI();
        }
    }
}
