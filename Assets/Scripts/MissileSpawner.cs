using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileSpawner : MonoBehaviour
{
    [SerializeField] private GameObject missilePrefab;

    public void SpawnMissile(Transform target)
    {
        Vector3 spawnPos = transform.TransformPoint(Vector3.forward * 10f);
        GameObject missile = Instantiate(missilePrefab, spawnPos, transform.rotation);
        missile.GetComponent<Missile>().target = target;
    }
}
