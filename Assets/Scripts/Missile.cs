using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour
{
    public Transform target;
    private Vector3 directionToTarget;
    private Rigidbody _body;

    public float speed = 3000f;
    public float rotationSpeed = 20f;
    public float rotationRange = 0.5f;
    public int damage = 50;

    [SerializeField] private GameObject hitBrefab;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip launchSound;
    [SerializeField] private AudioClip explodeSound;

    void Start()
    {
        _body = GetComponent<Rigidbody>();
        audioSource.PlayOneShot(launchSound);

        Destroy(gameObject, 10f);
    }

    void Update()
    {
        if (target == null) return;

        directionToTarget = target.position - transform.position;
    }

    void FixedUpdate()
    {
        if (target == null) return;

        float thrust = Vector3.Dot(directionToTarget.normalized, transform.forward);
        float rotation = Vector3.Dot(directionToTarget.normalized, transform.right);

        thrust = Mathf.Clamp(thrust, 0, 1);
        rotation = Mathf.Clamp(rotation, -rotationRange, rotationRange);

        float rotationAmount = rotation * rotationSpeed * (thrust < 0.2f ? rotationSpeed * 3 : rotationSpeed) * Time.deltaTime;
        transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y + rotationAmount, 0);

        _body.velocity = transform.forward * thrust * speed * Time.deltaTime;
    }

    void OnCollisionEnter(Collision collision)
    {
        audioSource.PlayOneShot(explodeSound);

        EnemyMovement enemy = collision.contacts[0].otherCollider.GetComponent<EnemyMovement>();
        if (enemy != null) enemy.TakeDamage(damage);

        Instantiate(hitBrefab, transform.position, Quaternion.identity);

        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<Rigidbody>().mass = 0;
        GetComponent<BoxCollider>().enabled = false;
        Destroy(transform.GetChild(0).gameObject);

        Destroy(gameObject, 1.2f);
    }
}
