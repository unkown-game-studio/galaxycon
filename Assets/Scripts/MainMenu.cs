using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject settingsMenu;
    public GameObject mainMenu;

    public Slider soundSlider;
    public Slider musicSlider;
    public AudioSource musicSource;

    void Start()
    {
        musicSource.ignoreListenerVolume = true;
        musicSlider.value = PlayerPrefs.GetFloat("musicvolume", 0.05f);
        musicSource.volume = musicSlider.value;

        soundSlider.value = PlayerPrefs.GetFloat("soundvolume", 1f);
    }

    public void OnStartButton() {
        SceneManager.LoadScene("Game");
    }

    public void OpenSettings()
    {
        mainMenu.SetActive(false);
        settingsMenu.SetActive(true);
    }

    public void CloseSettings()
    {
        mainMenu.SetActive(true);
        settingsMenu.SetActive(false);
    }

    public void OnSoundSliderValue()
    {
        PlayerPrefs.SetFloat("soundvolume", soundSlider.value);
    }

    public void OnMusicSliderValue()
    {
        musicSource.volume = musicSlider.value;
        PlayerPrefs.SetFloat("musicvolume", musicSlider.value);
    }
}
