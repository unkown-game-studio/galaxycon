using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    [SerializeField] private GameObject explosionVFX;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip explosionSound;
    [SerializeField] private AudioClip placeSound;

    public float explosionDelay = 1.5f;
    public float radius = 50f;
    public int damage = 100;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        StartCoroutine(ExplodeRoutine());
    }

    IEnumerator ExplodeRoutine()
    {
        audioSource.PlayOneShot(placeSound);

        yield return new WaitForSeconds(explosionDelay);

        Instantiate(explosionVFX, transform.position, Quaternion.identity);
        audioSource.PlayOneShot(explosionSound);

        Collider[] nearObjects = Physics.OverlapSphere(transform.position, radius);

        foreach (Collider collider in nearObjects)
        {
            EnemyMovement enemy = collider.transform.GetComponent<EnemyMovement>();
            Asteroid asteroid = collider.transform.GetComponent<Asteroid>();

            if (enemy != null) enemy.TakeDamage(damage);
            if (asteroid != null) asteroid.TakeDamage(10);
        }

        GetComponent<MeshRenderer>().enabled = false;

        Destroy(gameObject, 2);
    }
}
