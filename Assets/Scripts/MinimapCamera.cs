using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapCamera : MonoBehaviour
{
    [SerializeField] private Player player;

    void LateUpdate()
    {
        float oldY = transform.position.y;
        transform.position = new Vector3(player.transform.position.x, oldY, player.transform.position.z);
    }
}