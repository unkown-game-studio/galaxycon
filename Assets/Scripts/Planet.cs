using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour
{

    public int level, defenderCount;
    public bool isCaptured = false;

    public List<EnemyMovement> defenders = new List<EnemyMovement>();
    public Transform[] patrolWaypoints;
    public Transform[] spawnPoints;
    public EnemyMovement.ZoneType zoneType;

    [SerializeField] private Player player;
    [SerializeField] private SpriteRenderer mapIcon, minimapIcon;
    [SerializeField] private Color coldZoneColor, hotZoneColor;

    private WandererEnemyManager enemyManager;
    public GameObject zone;
    public GameObject dock;
    private GameObject _defenderPrefab;

    public float timeToReassignWayPoints = 5f;
    private float timePassed = 0f;

    void Start()
    {
        enemyManager = GameObject.Find("WandererEnemyManager").GetComponent<WandererEnemyManager>();

        if(!isCaptured)
        {
            _defenderPrefab = enemyManager.prefabs[level - 1];
            SpawnDefenders();

            if (zoneType == EnemyMovement.ZoneType.Cold)
            {
                mapIcon.color = coldZoneColor;
                minimapIcon.color = coldZoneColor;
            }
            else if (zoneType == EnemyMovement.ZoneType.Hot)
            {
                mapIcon.color = hotZoneColor;
                minimapIcon.color = hotZoneColor;
            }
        }
        else
        {
            CapturePlanet();
        }
    }
    void Update()
    {
        if(!isCaptured)
        {
            timePassed += Time.deltaTime;
            if (timePassed >= timeToReassignWayPoints)
            {
                timePassed = 0f;
                foreach (EnemyMovement enemy in defenders)
                {
                    if (!enemy.targetObject.CompareTag("Player"))
                        enemy.targetObject = GetWayPoint();
                }
            }

            if(defenders.Count == 0)
            {
                CapturePlanet();
            }
        }
    }

    private void CapturePlanet()
    {
        isCaptured = true;
        zone.SetActive(false);
        dock.SetActive(true);
        mapIcon.color = Color.green;
        minimapIcon.color = Color.green;
        player.score += level * 1000;
    }

    private void SpawnDefenders()
    {
        for(int i = 0; i < defenderCount; ++i)
        {
            GameObject enemyInstance = Instantiate(_defenderPrefab, spawnPoints[i].position, spawnPoints[i].rotation);
            EnemyMovement enemy = enemyInstance.GetComponent<EnemyMovement>();
            enemy.type = EnemyMovement.Type.Defender;
            enemy.targetObject = GetWayPoint();
            enemy.planet = gameObject.GetComponent<Planet>();
            enemy.zoneType = zoneType;
            defenders.Add(enemy);
        }
    }

    public Transform GetWayPoint()
    {
        return patrolWaypoints[(int)Random.Range(0, patrolWaypoints.Length - 1)];
    }

    public void PlayerEnteredZone()
    {
        Player player = GameObject.Find("Player").GetComponent<Player>();
        foreach (EnemyMovement enemy in defenders)
        {
            enemy.targetObject = player.transform;
            if(!player.chasers.Contains(enemy))
                player.chasers.Add(enemy);
        }
    }

}
