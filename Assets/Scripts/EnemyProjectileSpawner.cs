using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectileSpawner : MonoBehaviour
{
    [SerializeField] private GameObject projectilePrefab;
    [SerializeField] private AudioClip blasterSound;
    [SerializeField] private AudioSource soundSource;

    public void Spawn(int damage, Quaternion rotation)
    {
        soundSource.PlayOneShot(blasterSound);

        GameObject projectile = Instantiate(projectilePrefab);
        projectile.GetComponent<MovingProjectile>().damage = damage;
        projectile.layer = LayerMask.NameToLayer("Player");
        projectile.transform.position = transform.TransformPoint(Vector3.forward * 3f);
        projectile.transform.rotation = rotation;
    }
}
