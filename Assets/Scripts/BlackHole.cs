using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHole : MonoBehaviour
{
    public List<Rigidbody> pulledBodies = new List<Rigidbody>();
    public float pullForce = 1f;

    void FixedUpdate()
    {
        foreach(Rigidbody body in pulledBodies)
        {
            Vector3 direction = (transform.position - body.transform.position).normalized;
            body.AddForce(direction * pullForce);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Player player = other.GetComponent<Player>();
        EnemyMovement enemy = other.GetComponent<EnemyMovement>();

        if (player != null && !pulledBodies.Contains(player.GetComponent<Rigidbody>())) pulledBodies.Add(player.GetComponent<Rigidbody>());
        if (enemy != null && !pulledBodies.Contains(enemy.GetComponent<Rigidbody>())) pulledBodies.Add(enemy.GetComponent<Rigidbody>());
    }
}
